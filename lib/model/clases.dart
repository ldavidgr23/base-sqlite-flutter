class Producto {
  int id;
  String img;
  String nombre;
  String descripcion;
  String  precio;
  String largo;
  String ancho;
  String moneda;


 Producto ({this.id, this.img, this.nombre, this.descripcion,this.precio,  this.largo, this.ancho,this.moneda});

  //To insert the data in the bd, we need to convert it into a Map
  //Para insertar los datos en la bd, necesitamos convertirlo en un Map
  Map<String, dynamic> toMap() => {
    "id": id,
    "img": img,
    "nombre": nombre,
    "descripcion": descripcion,
    "precio": precio,
    "largo":largo,
    "ancho": ancho,    
    "moneda": moneda,
   
  };

  //to receive the data we need to pass it from Map to json
  //para recibir los datos necesitamos pasarlo de Map a json
  factory Producto.fromMap(Map<String, dynamic> json) => new Producto(
   id: json["id"],
   img : json["img"],
   nombre: json["nombre"],
   descripcion : json["descripcion"],
   precio : json["precio"],
   largo: json["largo"],
   ancho  : json["ancho"],
   moneda  : json["moneda"],
    
    );
}



