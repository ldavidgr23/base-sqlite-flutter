import 'package:flutter/material.dart';
import 'main.dart';
import 'model/clases.dart';

import 'package:primerparcial/model/clases.dart';
import 'package:primerparcial/database/database.dart';

class View extends StatefulWidget {
  final bool edit;
  final Producto product;

  View(this.edit, {this.product}) : assert(edit == true || product == null);

  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends State<View> {
  TextEditingController nombreEditingController = TextEditingController();
  TextEditingController descripcionEditingController = TextEditingController();
  TextEditingController imgEditingController = TextEditingController();
  TextEditingController precioEditingController = TextEditingController();
  TextEditingController largoEditingController = TextEditingController();
  TextEditingController anchoEditingController = TextEditingController();
  TextEditingController monedaEditingController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    //if you press the button to edit it must pass to true,
    //instantiate the name and phone in its respective controller, (link them to each controller)
    if (widget.edit == true) {
      nombreEditingController.text = widget.product.nombre;
      descripcionEditingController.text = widget.product.descripcion;
      imgEditingController.text = widget.product.img;
      precioEditingController.text = widget.product.precio;
      largoEditingController.text = widget.product.largo;
      anchoEditingController.text = widget.product.ancho;
      monedaEditingController.text = widget.product.moneda;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Mexi-Prod"),
      ),
      body: Container(
        key: _formKey,
        child: Center(
          child: Column(
            children: <Widget>[
              Card(
                elevation: 5,
                margin:
                    EdgeInsets.only(top: 15, right: 15, left: 15, bottom: 10),
                color: Colors.white,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    TextField(
                      enabled: false,
                      textAlign: TextAlign.center,
                      controller: nombreEditingController,
                      autofocus: false,
                      style: TextStyle(fontSize: 30.0, color: Colors.pink),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        hintText: 'Nombre:',
                      ),
                    ),
                    Image.asset(
                      "images/jarron.png",
                    ),
                    Container(
                      width: 330,
                      height: 100,
                      child: TextField(
                        enabled: false,
                        textAlign: TextAlign.center,
                        controller: descripcionEditingController,
                        autofocus: false,
                        style: TextStyle(fontSize: 14.0, color: Colors.pink),
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          hintText: 'Description:',
                        ),
                      ),
                    ),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 39,
                          margin: EdgeInsets.only(
                              top: 0, right: 0, left: 0, bottom: 0),
                          child: TextField(
                            enabled: false,
                            controller: largoEditingController,
                            autofocus: false,
                            style:
                                TextStyle(fontSize: 14.0, color: Colors.pink),
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              hintText: 'largo:',
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                              top: 0, right: 0, left: 0, bottom: 0),
                          child: Text(
                            "cm H  x",
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.grey,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ),
                        Container(
                          width: 45,
                          margin: EdgeInsets.only(
                              top: 0, right: 0, left: 0, bottom: 0),
                          child: TextField(
                            enabled: false,
                            controller: anchoEditingController,
                            autofocus: false,
                            style:
                                TextStyle(fontSize: 14.0, color: Colors.pink),
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              hintText: 'ancho:',
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                              top: 0, right: 0, left: 0, bottom: 0),
                          child: Text(
                            "cm Diam ",
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.grey,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.monetization_on,
                          color: Colors.teal[500],
                          size: 36.0,
                        ),
                        Container(
                          width: 70,                         
                          child: TextField(
                            enabled: false,
                            controller: precioEditingController,
                            autofocus: false,
                            style: TextStyle(
                                fontSize: 25.0, color: Colors.teal[500]),
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              hintText: 'Presio:',
                            ),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      width: 120,
                      height: 50,
                      margin: EdgeInsets.only(
                          top: 0, right: 0, left: 250, bottom: 35),
                      child: FlatButton(
                        color: Colors.teal[500],
                        splashColor: Colors.greenAccent,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                        child: Text(
                          "Comprar",
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.white,
                          ),
                        ),
                        onPressed: () {},
                      ),
                    ),
                  ],
                ),
              ),
              Row(
  mainAxisAlignment: MainAxisAlignment.center,
  children: const <Widget>[
    Icon(
      Icons.star,
      color: Colors.pinkAccent,
      size: 30.0,
     
    ),
    Icon(
      Icons.star,
      color: Colors.pinkAccent,
      size: 30.0,
    
    ),
     Icon(
      Icons.star,
      color: Colors.pinkAccent,
      size: 30.0,
      
    ),
     Icon(
      Icons.star,
      color: Colors.pinkAccent,
      size: 30.0,
     
    ),
     Icon(
      Icons.star_border,
      color: Colors.pinkAccent,
      size: 30.0,
     
    ),
   
  ],
)
            ],
          ),
        ),
      ),
    );
  }
}
