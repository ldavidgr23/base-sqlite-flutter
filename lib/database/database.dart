import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:primerparcial/model/clases.dart';
import 'package:sqflite/sql.dart';
import 'package:sqflite/sqlite_api.dart';

class ProductoDatabaseProvider{
  ProductoDatabaseProvider._();

  static final ProductoDatabaseProvider db = ProductoDatabaseProvider._();
  Database _database;


  Future<Database> get database async {
    if(_database != null) return _database;
    _database = await getDatabaseInstanace();
    return _database;
  }

  Future<Database> getDatabaseInstanace() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, "producto.db");
     return await openDatabase(path, version: 1,
      onCreate: (Database db, int version) async {
        await db.execute("CREATE TABLE Producto("
        "id integer primary key,"
        "img VARCHAR,"
        "nombre TEXT,"
        "descripcion TEXT,"
        "precio TEXT,"
        "largo TEXT,"
        "ancho TEXT,"
        "moneda TEXT"
        ")");
      });
  }

  //Query
  //muestra todos los productos de la base de datos
  Future<List<Producto>> getAllProducto() async {
    final db = await database;
    var response = await db.query("Producto");
    List<Producto> list = response.map((c) => Producto.fromMap(c)).toList();
    return list;
  }

  //Query
  //muestra un solo Producto por el id la base de datos
  Future<Producto> getProductotWithId(int id) async {
    final db = await database;
    var response = await db.query("Producto", where: "id = ?", whereArgs: [id]);
    return response.isNotEmpty ? Producto.fromMap(response.first) : null;
  }

  //Insert
  addProductoToDatabase(Producto producto) async {
    final db = await database;
    var table = await db.rawQuery("SELECT MAX(id)+1 as id FROM Producto");
    int id = table.first["id"];
    producto.id = id;
    var raw = await db.insert(
      "Producto",
      producto.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return raw;

  }

  //Delete
  //Delete Producto with id
  deleteProductoWithId(int id) async {
    final db = await database;
    return db.delete("Producto", where: "id = ?", whereArgs: [id]);
  } 

  //Delete all Products
  deleteAllProducto() async {
    final db = await database;
    db.delete("Producto");
  } 

  //Update
  updateProducto(Producto producto) async {
    final db = await database;
    var response = await db.update("Producto", producto.toMap(),
    where: "id = ?", whereArgs: [producto.id]);
    return response;
  }

}