import 'package:flutter/material.dart';
import 'package:primerparcial/view.dart';
import 'second.dart';
import 'package:primerparcial/model/clases.dart';
import 'package:primerparcial/database/database.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Mexi-Prod',
      theme: ThemeData(
        primarySwatch: Colors.pink,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void didUpdateWidget(MyHomePage oldWidget) {
    super.didUpdateWidget(oldWidget);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Mexi-Prod"),
      ),
      body: FutureBuilder<List<Producto>>(
        future: ProductoDatabaseProvider.db.getAllProducto(),
        builder:
            (BuildContext context, AsyncSnapshot<List<Producto>> snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              physics: BouncingScrollPhysics(),
              //Count all records
              itemCount: snapshot.data.length,
              //all the records that are in the Producto table are passed to an item Producto item = snapshot.data [index];
              itemBuilder: (BuildContext context, int index) {
                Producto item = snapshot.data[index];
                //delete one register for id
                return Dismissible(
                  key: UniqueKey(),
                  background: Container(color: Colors.grey),
                  onDismissed: (diretion) {
                    //incerta Alerta de eliminar
                    ProductoDatabaseProvider.db.deleteProductoWithId(item.id);
                  },
                  confirmDismiss: (DismissDirection direction) async {
                    return await showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          content: const Text(
                            "¿Do you want to logout?",
                            style: TextStyle(
                              fontSize: 30,
                              color: Colors.pinkAccent,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          actions: <Widget>[
                            FlatButton(
                              color: Colors.white,
                              textColor: Colors.green,
                              disabledColor: Colors.greenAccent,
                              splashColor: Colors.teal[500],
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: Text(
                                "Cancel",
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.teal[500],
                                ),
                              ),
                              onPressed: () => Navigator.of(context).pop(false),
                            ),
                            FlatButton(
                              color: Colors.teal[500],
                              splashColor: Colors.greenAccent,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: Text(
                                "Confirm",
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                              onPressed: () => Navigator.of(context).pop(true),
                            ),
                          ],
                        );
                      },
                    );
                  },
                  //Now we paint the list with all the records, which will have a name,...

                  child: Card(
                    elevation: 8,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(9.0)),
                    child: Column(children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(
                            top: 0, right: 80, left: 0, bottom: 0),
                        child: Text(
                          item.nombre,
                          style: TextStyle(
                            fontSize: 30,
                            color: Colors.pinkAccent,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            top: 0, right: 70, left: 0, bottom: 0),
                        child: Text(
                          item.descripcion,
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(
                                top: 0, right: 0, left: 100, bottom: 0),
                            child: Text(
                              item.largo,
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.grey,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                top: 0, right: 0, left: 0, bottom: 0),
                            child: Text(
                              "cm H x ",
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.grey,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                top: 0, right: 0, left: 0, bottom: 0),
                            child: Text(
                              item.ancho,
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.grey,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                top: 0, right: 0, left: 0, bottom: 0),
                            child: Text(
                              "cm Diam",
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.grey,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                top: 0, right: 0, left: 30, bottom: 0),
                            child: Image.asset(
                              "images/jarron.png",
                              height: 100,
                              width: 100,
                              fit: BoxFit.fitWidth,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          InkWell(
                            child: Container(
                              margin: EdgeInsets.only(
                                  top: 0, right: 0, left: 50, bottom: 0),
                              child: Image.asset(
                                "images/icon_eye.png",
                                height: 30,
                                width: 30,
                                fit: BoxFit.fitWidth,
                              ),
                            ),
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => View(
                                        true,
                                        //Here is the record that we want to edit
                                        product: item,
                                      )));
                            },
                          ),
                          InkWell(
                            child: Container(
                              margin: EdgeInsets.only(
                                  top: 0, right: 0, left: 50, bottom: 0),
                              child: Image.asset(
                                "images/icon_edit.png",
                                height: 30,
                                width: 30,
                                fit: BoxFit.fitWidth,
                              ),
                            ),
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => Second(
                                        true,
                                        //Here is the record that we want to edit
                                        product: item,
                                      )));
                            },
                          ),
                          InkWell(
                            child: Container(
                              margin: EdgeInsets.only(
                                  top: 0, right: 0, left: 50, bottom: 10),
                              child: Image.asset(
                                "images/icon_trash.png",
                                height: 30,
                                width: 30,
                                fit: BoxFit.fitWidth,
                              ),
                            ),
                            onTap: () {
                              ProductoDatabaseProvider.db
                                  .deleteProductoWithId(item.id);
                            },
                          ),
                        ],
                      ),
                    ]),
                  ),
                );
              },
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
      //This button takes us to the method add new register, which is in the file add_editclient.dart
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => Second(false)));
        },
      ),
    );
  }
}
