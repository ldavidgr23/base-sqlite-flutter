import 'dart:io';

import 'package:flutter/material.dart';
import 'main.dart';
import 'model/clases.dart';
import 'package:image_picker/image_picker.dart';
import 'package:primerparcial/model/clases.dart';
import 'package:primerparcial/database/database.dart';

class Second extends StatefulWidget {
  final bool edit;
  final Producto product;

  Second(this.edit, {this.product}) : assert(edit == true || product == null);

  @override
  _SecondState createState() => _SecondState();
}

class _SecondState extends State<Second> {
  Future<File> imageFile;

  TextEditingController nombreEditingController = TextEditingController();
  TextEditingController descripcionEditingController = TextEditingController();
  TextEditingController imgEditingController = TextEditingController();
  TextEditingController precioEditingController = TextEditingController();
  TextEditingController largoEditingController = TextEditingController();
  TextEditingController anchoEditingController = TextEditingController();
  TextEditingController monedaEditingController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

//drop
  String _value = null;
  List<String> _values = new List<String>();

  void _onChanged(String value) {
    setState(() {
      _value = value;
    });
  }

//drop
  @override
  void initState() {
    super.initState();
    //if you press the button to edit it must pass to true,
    //instantiate the name and phone in its respective controller, (link them to each controller)
    if (widget.edit == true) {
      nombreEditingController.text = widget.product.nombre;
      descripcionEditingController.text = widget.product.descripcion;
      imgEditingController.text = widget.product.img;
      precioEditingController.text = widget.product.precio;
      largoEditingController.text = widget.product.largo;
      anchoEditingController.text = widget.product.ancho;
      monedaEditingController.text = widget.product.moneda;
    }
    _values.addAll(["Peso MX", "Euro", "Dolar"]);
    _value = _values.elementAt(0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Mexi-Prod"),
      ),
      body: Container(
        key: _formKey,
        child: Center(
          child: ListView(
            children: <Widget>[
              Container(
                margin:
                    EdgeInsets.only(top: 5, right: 15, left: 15, bottom: 10),
                child: Text(
                  widget.edit ? "Modificar producto" : "Nuevo producto",
                  style: TextStyle(
                    color: Colors.pink,
                    fontSize: 30,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
              Container(
                margin:
                    EdgeInsets.only(top: 35, right: 15, left: 15, bottom: 10),
                child: TextField(
                  controller: nombreEditingController,
                  autofocus: false,
                  style: TextStyle(fontSize: 22.0, color: Colors.pink),
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    hintText: 'Nombre:',
                    contentPadding: const EdgeInsets.only(
                        left: 14.0, bottom: 8.0, top: 8.0),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.pink),
                      borderRadius: BorderRadius.circular(25.7),
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.pink),
                      borderRadius: BorderRadius.circular(25.7),
                    ),
                  ),
                ),
              ),
              Card(
                  elevation: 5,
                  margin:
                      EdgeInsets.only(top: 15, right: 15, left: 15, bottom: 10),
                  color: Colors.white,
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: TextField(
                      controller: descripcionEditingController,
                      maxLines: 8,
                      decoration:
                          InputDecoration.collapsed(hintText: "Descripción"),
                    ),
                  )),
              Row(
                children: <Widget>[
                  Container(
                    width: 120,
                    height: 50,
                    margin: EdgeInsets.only(
                        top: 15, right: 80, left: 30, bottom: 10),
                    child: TextField(
                      keyboardType: TextInputType.number,
                      controller: precioEditingController,
                      autofocus: false,
                      style:
                          TextStyle(fontSize: 22.0, color: Colors.pinkAccent),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        hintText: 'Costo',
                        contentPadding: const EdgeInsets.only(
                            left: 14.0, bottom: 8.0, top: 8.0),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.pinkAccent),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.pinkAccent),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: 120,
                    height: 50,
                    margin: EdgeInsets.only(
                        top: 15, right: 0, left: 10, bottom: 10),
                    child: Column(
                      children: <Widget>[
                        new DropdownButton(
                          value: _value,
                          items: _values.map((String value) {
                            return new DropdownMenuItem(
                                value: value,
                                child: new Row(
                                  children: <Widget>[
                                    new Icon(Icons.monetization_on),
                                    new Text('${value}')
                                  ],
                                ));
                          }).toList(),
                          onChanged: (String value) {
                            _onChanged(value);
                          },
                        )
                      ],
                    ),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                    width: 120,
                    height: 50,
                    margin: EdgeInsets.only(
                        top: 15, right: 80, left: 30, bottom: 10),
                    child: TextField(
                      keyboardType: TextInputType.number,
                      controller: largoEditingController,
                      autofocus: false,
                      style:
                          TextStyle(fontSize: 22.0, color: Colors.pinkAccent),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        hintText: 'Largo',
                        contentPadding: const EdgeInsets.only(
                            left: 14.0, bottom: 8.0, top: 8.0),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.pinkAccent),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.pinkAccent),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: 120,
                    height: 50,
                    margin: EdgeInsets.only(
                        top: 15, right: 0, left: 10, bottom: 10),
                    child: TextField(
                      keyboardType: TextInputType.number,
                      controller: anchoEditingController,
                      autofocus: false,
                      style:
                          TextStyle(fontSize: 22.0, color: Colors.pinkAccent),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        hintText: 'Ancho',
                        contentPadding: const EdgeInsets.only(
                            left: 14.0, bottom: 8.0, top: 8.0),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.pinkAccent),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.pinkAccent),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                margin:
                    EdgeInsets.only(top: 10, right: 20, left: 20, bottom: 10),
                child: FlatButton(
                  color: Colors.pinkAccent,
                  textColor: Colors.white,
                  disabledColor: Colors.grey,
                  splashColor: Colors.greenAccent,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: Text(
                    "Imagen",
                    style: TextStyle(
                      fontSize: 15,
                      color: Colors.white,
                    ),
                  ),
                  onPressed: () {},
                ),
              ),
              Row(
                children: <Widget>[
                  Container(
                    width: 120,
                    height: 50,
                    margin: EdgeInsets.only(
                        top: 30, right: 30, left: 55, bottom: 35),
                    child: FlatButton(
                      color: Colors.white,
                      textColor: Colors.green,
                      disabledColor: Colors.greenAccent,
                      splashColor: Colors.teal[500],
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Text(
                        "Limpiar",
                        style: TextStyle(
                          fontSize: 15,
                          color: Colors.teal[500],
                        ),
                      ),
                      onPressed: () {
                        nombreEditingController.text = " ";
                        descripcionEditingController.text = " ";
                        imgEditingController.text = " ";
                        precioEditingController.text = " ";
                        largoEditingController.text = " ";
                        anchoEditingController.text = " ";
                        monedaEditingController.text = " ";
                      },
                    ),
                  ),
                  Container(
                    width: 120,
                    height: 50,
                    margin: EdgeInsets.only(
                        top: 30, right: 80, left: 5, bottom: 35),
                    child: FlatButton(
                      color: Colors.teal[500],
                      splashColor: Colors.greenAccent,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Text(
                        "Guardar",
                        style: TextStyle(
                          fontSize: 15,
                          color: Colors.white,
                        ),
                      ),
                      onPressed: () async {
                        if (nombreEditingController.text.isEmpty) {
                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              title: Text('Ops!'),
                              content: Text('Complete el nombre'),
                            ),
                          );
                        } else {
                          if (descripcionEditingController.text.isEmpty) {
                            showDialog(
                              context: context,
                              builder: (context) => AlertDialog(
                                title: Text('Ops!'),
                                content: Text('Falta descripcion'),
                              ),
                            );
                          } else {
                            if (precioEditingController.text.isEmpty) {
                              showDialog(
                                context: context,
                                builder: (context) => AlertDialog(
                                  title: Text('Ops!'),
                                  content: Text('Falta un precio'),
                                ),
                              );
                            } else {
                              if (largoEditingController.text.isEmpty) {
                                showDialog(
                                  context: context,
                                  builder: (context) => AlertDialog(
                                    title: Text('Ops!'),
                                    content: Text('Cual es su largo'),
                                  ),
                                );
                              }
                              if (anchoEditingController.text.isEmpty) {
                                showDialog(
                                  context: context,
                                  builder: (context) => AlertDialog(
                                    title: Text('Ops!'),
                                    content: Text('Cual es su ancho'),
                                  ),
                                );
                              } else {
                                if (int.parse(largoEditingController.text) <
                                    1) {
                                  showDialog(
                                    context: context,
                                    builder: (context) => AlertDialog(
                                      title: Text('Ops!'),
                                      content: Text('Ingrese un tamaño valido'),
                                    ),
                                  );
                                } else {
                                  if (int.parse(anchoEditingController.text) <
                                      1) {
                                    showDialog(
                                      context: context,
                                      builder: (context) => AlertDialog(
                                        title: Text('Ops!'),
                                        content:
                                            Text('Ingrese un tamaño valido'),
                                      ),
                                    );
                                  } else {
                                    if (int.parse(
                                            precioEditingController.text) <
                                        1) {
                                      showDialog(
                                        context: context,
                                        builder: (context) => AlertDialog(
                                          title: Text('Ops!'),
                                          content: Text('Ingrese costo valido'),
                                        ),
                                      );
                                    } else {
                                      if (widget.edit == true) {
                                        ProductoDatabaseProvider.db
                                            .updateProducto(new Producto(
                                                nombre: nombreEditingController
                                                    .text,
                                                descripcion:
                                                    descripcionEditingController
                                                        .text,
                                                // img: imgEditingController.text,
                                                precio: precioEditingController
                                                    .text,
                                                largo:
                                                    largoEditingController.text,
                                                ancho:
                                                    anchoEditingController.text,
                                                moneda: monedaEditingController
                                                    .text,
                                                id: widget.product.id));
                                        Navigator.of(context).push(
                                          MaterialPageRoute(
                                            builder: (context) => MyApp(),
                                          ),
                                        );
                                      } else {
                                        await ProductoDatabaseProvider.db
                                            .addProductoToDatabase(new Producto(
                                          nombre: nombreEditingController.text,
                                          descripcion:
                                              descripcionEditingController.text,
                                          // img: imgEditingController.text,
                                          precio: precioEditingController.text,
                                          largo: largoEditingController.text,
                                          ancho: anchoEditingController.text,
                                          moneda: monedaEditingController.text,
                                        ));
                                        Navigator.of(context).push(
                                          MaterialPageRoute(
                                            builder: (context) => MyApp(),
                                          ),
                                        );
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void getFile() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      widget.product.img = image.path;
    });
  }
}
